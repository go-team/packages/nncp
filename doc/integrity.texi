@node Integrity
@cindex integrity check
@cindex authenticity check
@cindex OpenPGP
@cindex gpg
@cindex GnuPG
@cindex WKD
@cindex OpenSSH
@section Tarballs integrity check

You @strong{have to} verify downloaded tarballs authenticity to be sure
that you retrieved trusted and untampered software. There are two options:

@table @asis

@item @url{https://www.openpgp.org/, OpenPGP} @file{.asc} signature
    Use @url{https://www.gnupg.org/, GNU Privacy Guard} free software
    implementation.
    For the very first time it is necessary to get signing public key and
    import it. It is provided @url{.well-known/openpgpkey/nncpgo.org/hu/i4cdqgcarfjdjnba6y4jnf498asg8c6p.asc, here}, but you should
    check alternate resources.

@verbatim
pub   rsa2048/0x2B25868E75A1A953 2017-01-10
      92C2 F0AE FE73 208E 46BF  F3DE 2B25 868E 75A1 A953
uid   NNCP releases <releases at nncpgo dot org>
@end verbatim

@example
$ gpg --auto-key-locate dane --locate-keys releases at nncpgo dot org
$ gpg --auto-key-locate  wkd --locate-keys releases at nncpgo dot org
@end example

@item @url{https://www.openssh.com/, OpenSSH} @file{.sig} signature
    @url{PUBKEY-SSH.pub, Public key} and its OpenPGP
    @url{PUBKEY-SSH.pub.asc, signature} made with the key above.
    Its fingerprint: @code{SHA256:FRiWawVNBkyS3jFn8uZ/JlT+PWKSFbhWe5XSixp1+SY}.

@example
$ ssh-keygen -Y verify -f PUBKEY-SSH.pub -I releases@@nncpgo.org -n file \
    -s nncp-@value{VERSION}.tar.zst.sig <nncp-@value{VERSION}.tar.zst
@end example

@end table
